#lang racket

(provide call-with-warning-handler
         raise-warning)

(struct warning-exception
  ((warning-message)
   (continuation)))

(define (call-with-warning-handler warn-proc exec-proc)
  (call-with-exception-handler
    (λ (exc)
      (if (warning-exception? exc)
          (begin
            (warn-proc (warning-exception-warning-message exc)
                       (warning-exception-continuation exc))
            (warning-exception-warning-message exc))
          exc))
    exec-proc))

(define (raise-warning warn)
  (call-with-current-continuation
    (λ (cont)
      (raise (warning-exception warn
                                (λ () (cont)))))))
