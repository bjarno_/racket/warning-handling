# Warning Handling

This is a prototype implementation of `call-with-warning-handler` and `throw-warning`.

## Rationale

In some situations in Racket, it might be useful to raise warnings instead of errors or exceptions.

Warnings are inherintly different from exceptions. Warnings and exceptions are both useful to signal to the lexical caller that a certain erronous state has been encountered in an algorithm. However, and this is what makes warnings different from exceptions, this erronous state can sometimes be ignored. E.g., adding an value to a set which already contains this value should raise a warning (e.g., the element was already in the set) instead of doing nothing (which might be useful for sets, but can indicate that the algorithm implemented using set addition might not be correct), or raising an error (which would be too strict).

Thus, warnings can be used in these scenarios, making it possible for the calling environment to either ignore the warning (and to continue to computation), or to handle the warning as an error explicitely.

## Usage

1. Clone this repository
2. Include the `warning-handling.rkt` file of this repository

In order to raise a warning, make use of `(raise-warning val)`. Its sole argument should be the value that contains information about the warning, and can be anything.

Make use of `(call-with-warning-handler warn-proc exec-proc)` to call another procedure with your own warning handler. `warn-proc` should expect two arguments: one is the warning value (signaled using `raise-warning`), the other is a continue procedure which resumes the original computation. `warn-proc` should call `continue` to immediately resume the original computation, and abort the warning from being processed. `exec-proc` should be a thunk, which gets called using `warn-proc` as the current warning handler.

Note that when a warning is thrown, and no warning handler is active, the current exception handler will receive a opaque value.

## Example

**TODO: Add Link**

`test.rkt` contains an example that makes use of `warning-handling.rkt`.

## Implementation

This feature has been implemented by combining `call-with-exception-handler` with `call-with-current-continuation`.

When a warning is raised using `raise-warning`, the current continuation is captured, and sent together with the warning value to the current warning handler.
The warning handler receives both the warning value, and the continuation. When the continuation is not used in the warning handler, the warning is treated as an error by returning it in the exception handler that called the warning handler.
